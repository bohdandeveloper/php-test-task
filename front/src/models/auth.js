import config from "../../config"
import {ApiResponse, notify, NOTIFY_ERROR} from "../helpers"
import axios from "axios"

/**
 * @param {object} data
 * @returns {Promise<ApiResponse>}
 */
export function AdminRegistration(data) {
    return axios.request({
        method: 'post',
        url: config.baseApiUrl + 'auth/admin-registration',
        data,
        validateStatus: () => true,

    }).then(response => new ApiResponse(response))
        .then(response => {
            if (response.isCriticalOrHttpError()) {
                notify(NOTIFY_ERROR, 'Something went wrong: ' + response.errorMessage())
                throw new Error(response.errorMessage())
            }

            return response;
        });
}
