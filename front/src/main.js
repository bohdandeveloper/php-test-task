import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueMaterial from 'vue-material'
import routes from "./routes"
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueMaterial)

const router = new VueRouter({
    routes
})

new Vue({
    render: h => h(App),
    router,
    store: import("./store")
}).$mount('#app')
