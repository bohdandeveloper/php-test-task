export default {
    /**
     * Authenticated user
     */
    user: {
        status: false,
        token: null,
        identity: null,
    },
}
