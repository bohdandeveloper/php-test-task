export default {
    userAuthenticate: state => {
        return state.user.status
    },

    userIsAdmin: state => {
        return state.user.identity && state.user.identity.role === 'admin'
    },

    user: state => {
        return state.user.identity
    },

    authToken: state => {
        return state.user.token
    }
}
