import Vuex from "vuex"

import state from "./state"
import mutations from "./mutations"
import getters from "./getters"

export const store = new Vuex.Store({
    mutations,
    state,
    getters,
})
