export default {
    authenticateUser(state, {token, userIdentity}) {
        state.user.status = true;
        state.user.token = token;
        state.user.identity = userIdentity;
    },
}
