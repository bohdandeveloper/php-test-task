import MainPage from "./pages/MainPage";
import NotFound from "./pages/NotFound";
import AdminRegistrationPage from "./pages/AdminRegistrationPage";

export default [
    {path: '/', component: MainPage},
    {path: '/admin-registration', component: AdminRegistrationPage},
    {path: '*', component: NotFound},
];
