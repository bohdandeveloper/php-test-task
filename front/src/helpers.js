import alertifyjs from "alertifyjs";
import "alertifyjs/build/css/alertify.min.css"

/**
 * Api response wrapper allows to easy handling response status
 */
export class ApiResponse {
    constructor(axiosResponse) {
        this.status = axiosResponse.data.status;
        this.data = axiosResponse.data.data;
        this.error = axiosResponse.data.error;
    }

    isSuccess() {
        return this.status === 'ok';
    }

    isError() {
        return this.error !== null;
    }

    isCriticalOrHttpError() {
        return this.error && ['critical', 'http'].includes(this.error.type);
    }

    isValidationError() {
        return this.error && this.error.type === 'validation';
    }

    validationErrors() {
        return this.error ? this.error.validationErrors : null;
    }

    data() {
        return this.data;
    }
}

export const NOTIFY_SUCCESS = 'success';
export const NOTIFY_ERROR = 'error';

/**
 * Shows an alert on the page
 * @param {string} type
 * @param {string} message
 */
export function notify(type, message) {
    switch (type) {
        case NOTIFY_ERROR:
            alertifyjs.error(message);
            break;

        case NOTIFY_SUCCESS:
            alertifyjs.success(message);
            break;

        default:
            throw new Error('Unknown notification type: ' + type);
    }
}
