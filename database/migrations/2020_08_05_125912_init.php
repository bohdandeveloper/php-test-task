<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('role');
            $table->unsignedInteger('organization_id')->nullable();
            $table->timestamps();

            $table->foreign('organization_id')->references('id')->on('organizations');
        });

        Schema::create('organizations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('user_admin_id');
            $table->timestamps();

            $table->foreign('user_admin_id')->references('id')->on('users');
        });

        Schema::create('invites', function(Blueprint $table) {
            $table->increments('id');
            $table->uuid('invite_key')->unique();
            $table->string('email');
            $table->unsignedInteger('organization_id');
            $table->timestamps();

            $table->foreign('organization_id')->references('id')->on('organizations');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::drop('users');
        Schema::drop('organizations');
        Schema::drop('invites');

        Schema::enableForeignKeyConstraints();
    }
}
