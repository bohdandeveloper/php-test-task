# Test task

## Local setup
- `cp .env.example .env`
- Edit `.env` file, set proper database credentials
- `php artisan migrage`
- `cd ./front && npm install`
- `cp ./front/config.example.js ./front/config.js`
- Edit `./front/config.js` and set proper api url

## Local run
- `php artisan serve` to run api
- `npm run serve` to run frontend app
