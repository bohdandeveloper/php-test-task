<?php

namespace App\Traits;

trait ResponseTrait
{
    public function success(array $data, int $httpStatus = 200)
    {
        return response()->json([
            'status' => 'ok',
            'error' => null,
            'data' => $data,
        ])->setStatusCode($httpStatus);
    }

    public function error(int $httpStatus, string $type, string $message, array $errorAdditional = [])
    {
        return response()->json([
            'status' => 'error',
            'error' => array_merge([
                'type' => $type,
                'message' => $message,
            ], $errorAdditional),
            'data' => null,
        ])->setStatusCode($httpStatus);
    }
}
