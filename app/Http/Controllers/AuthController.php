<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminRegistrationRequest;
use App\Models\User;

class AuthController extends Controller
{
    public function adminRegistration(AdminRegistrationRequest $request)
    {
        $user = new User();
        $user->fill([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => \Hash::make($request->get('password')),
            'role' => User::ROLE_ADMIN,
        ]);
        $user->saveOrFail();

        return $this->success([
            'user' => $user,
        ]);
    }
}
